package jaeyoungchu.com.perpectdating.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import jaeyoungchu.com.perpectdating.BaseActivity;
import jaeyoungchu.com.perpectdating.ChatRoomActivity;
import jaeyoungchu.com.perpectdating.R;
import jaeyoungchu.com.perpectdating.adapter.RoomListViewAdapter;
import jaeyoungchu.com.perpectdating.helper.LastMsgTimeComparator;
import jaeyoungchu.com.perpectdating.model.ChatData;
import jaeyoungchu.com.perpectdating.model.ChatRoomData;
import jaeyoungchu.com.perpectdating.model.ProfileModel;

import static android.content.Context.MODE_PRIVATE;

public class ChatRoomListViewFragment extends Fragment {
    FirebaseUser user;
    View rootView;
    String deviceToken,opToken1;
    String opUserName,lastMsg,opToken;
    public DatabaseReference myDBRef;
    ArrayList<ChatRoomData> chatRoomDataList;
    RoomListViewAdapter adapter;
    ListView roomListView;
    ProfileModel pModel;
    ChatRoomData chatRoomData;
    ChatData cData;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

//        if(rootView != null){
//            return rootView;
//        }
        chatRoomData = new ChatRoomData();
        rootView = inflater.inflate(R.layout.tab_fragment_2, container, false);
        user = FirebaseAuth.getInstance().getCurrentUser();
        deviceToken = getUserToken();
        chatRoomDataList = new ArrayList<ChatRoomData>();
        roomListView = (ListView) rootView.findViewById(R.id.chat_room_listview);
        myDBRef = FirebaseDatabase.getInstance().getReferenceFromUrl("https://perpectdating.firebaseio.com/");
        downloadChatRoomList();

        return rootView;
    }





    private void downloadChatRoomList(){
        Query queryRef = myDBRef.child("chatroom").child(deviceToken);
        queryRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(final DataSnapshot snapshot, String previousChild) {

                myDBRef.child("chatroom").child(snapshot.getKey()).child(deviceToken).orderByChild("creationDate").limitToLast(1).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        cData = dataSnapshot.getValue(ChatData.class);
                        opToken1 = snapshot.getKey();
                        downloadOpDetails(cData,opToken1,cData.read);
                    }
                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        cData = dataSnapshot.getValue(ChatData.class);
                        opToken1 = snapshot.getKey();
                        downloadOpDetails(cData,opToken1,cData.read);
                    }
                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                    }
                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                myDBRef.child("chatroom").child(deviceToken).child(snapshot.getKey()).orderByChild("creationDate").limitToLast(1).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    }
                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }
                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        chatRoomDataList.clear();
                        adapter.notifyDataSetChanged();
                        roomListView.setAdapter(adapter);
                        downloadChatRoomList();
                    }
                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
            @Override
            public void onChildChanged(final DataSnapshot dataSnapshot2, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void downloadOpDetails(final ChatData cData, final String opToken1,final boolean isRead){

        chatRoomData = new ChatRoomData();
        chatRoomData.setRight(cData.isRight());
        chatRoomData.setOpUserToken(opToken1);
        chatRoomData.setRead(isRead);
        chatRoomData.setMsg(cData.getMsg());
        chatRoomData.setUserName(cData.getUserName());
        chatRoomData.setLastMsgTime(cData.creationDate);
        if (opToken1.equals(cData.getUserToken())){
            chatRoomData.setOpUserName(cData.getUserName());
            chatRoomData.setOpPhotoUrl(cData.getUserPhotoUrl());
        }else{
            chatRoomData.setOpUserName(cData.getOpUserName());
            chatRoomData.setOpPhotoUrl(cData.getOpUserPhotoUrl());
        }
        chatRoomData.setUserToken(deviceToken);
        boolean flag = false;

        if (chatRoomDataList.size() > 0){
            for (int i = 0 ; i < chatRoomDataList.size() ; i++){
                if ((chatRoomDataList.get(i).getUserToken().equals(chatRoomData.getUserToken()) && chatRoomDataList.get(i).getOpUserToken().equals(chatRoomData.getOpUserToken()))
                        ||(chatRoomDataList.get(i).getUserToken().equals(chatRoomData.getOpUserToken()) && chatRoomDataList.get(i).getOpUserToken().equals(chatRoomData.getUserToken()))){
                    chatRoomDataList.set(i,chatRoomData);
                    Collections.sort(chatRoomDataList, new LastMsgTimeComparator());
                    flag = true;
                }
            }
        }
        if (flag){
            adapter.notifyDataSetChanged();
            roomListView.setAdapter(adapter);
        }else{
            chatRoomDataList.add(chatRoomData);
            Collections.sort(chatRoomDataList, new LastMsgTimeComparator());
            initAdapter(chatRoomDataList);
        }
        flag = false;
    }


    @Override
    public void onStart() {
        super.onStart();
//        FirebaseDatabase.getInstance().goOnline();
    }

    @Override
    public void onResume() {
        super.onResume();

        downloadChatRoomList();
    }

    @Override
    public void onStop() {
        super.onStop();
//        FirebaseDatabase.getInstance().goOffline();
    }

    private void initAdapter(final ArrayList<ChatRoomData> roomList){
        if (getContext() != null){
            adapter = new RoomListViewAdapter(getContext(),roomList);
            roomListView.setAdapter(adapter);

            roomListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent toChatRoom = new Intent(getContext(),ChatRoomActivity.class);
                    toChatRoom.putExtra("opToken",roomList.get(position).getOpUserToken());
                    startActivity(toChatRoom);
                }
            });
        }
    }

    private String getUserToken(){
        if (user != null) {
            return user.getUid();
//            Log.d(TAG,"user Token " + userUid);
        }else {
            return null;
        }
    }
}