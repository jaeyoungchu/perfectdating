package jaeyoungchu.com.perpectdating.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import jaeyoungchu.com.perpectdating.R;
import jaeyoungchu.com.perpectdating.UserHomeProfileActivity;
import jaeyoungchu.com.perpectdating.adapter.GridAdapter;
import jaeyoungchu.com.perpectdating.helper.DistanceComparotor;
import jaeyoungchu.com.perpectdating.model.ProfileModel;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by chu on 2016-10-28.
 */

public class ListFragment extends Fragment {
    private final String TAG = "L ListFragment";
    private int index;
    GridView gridView;
    private StorageReference mStorage;
    private FirebaseUser user;
    private DatabaseReference myDBRef;
    private FirebaseDatabase database;
    private int currentScrollState;
    private ArrayList<ProfileModel> userList ,devidedUserList;;
    private ProgressDialog mProgressDialog;
    GridAdapter adapter;
    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(rootView != null){
            return rootView;
        }

        rootView = inflater.inflate(R.layout.tab_fragment_1, container, false);
        user = FirebaseAuth.getInstance().getCurrentUser();
        userList = new ArrayList<>();
        devidedUserList = new ArrayList<>();
        database = FirebaseDatabase.getInstance();
        myDBRef = database.getReference("profile");
        gridView = (GridView) rootView.findViewById(R.id.gridView);
        index = 0;
        if (userList.size() == 0) {
            downloadSortedProfile();
        }
        Log.d("check latancy ","after downloadSortedProfile()");
        return rootView;
    }

    private void downloadSortedProfile(){
        final ProfileModel myProfile = getPreferences();
        userList = new ArrayList<>();
        myDBRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userList.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    ProfileModel user = postSnapshot.getValue(ProfileModel.class);
                    if (myProfile != null){
                        float dis = getDistanceFromMe(myProfile.getLatitude(),myProfile.getLongitude(),user.getLatitude(),user.getLongitude());
                        user.setDistanceFromMe(dis);
                    }
//                    Log.d("2111","distance : " +dis);
                    if (user.getUid().equals(getUserToken())) {
                    } else {
                        userList.add(user);
                    }
                }
                Collections.sort(userList,new DistanceComparotor());
                for (int i=0; i < userList.size() ; i++){
                    Log.d("123","-----------user list name : -"+userList.get(i).getUsername());
                }
                initAdapter(userList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // ...
            }
        });
    }
    private float getDistanceFromMe(Double latA,Double longA,Double latB,Double longB){
        Location locationA = new Location("point A");

        locationA.setLatitude(latA);
        locationA.setLongitude(longA);

        Location locationB = new Location("point B");

        locationB.setLatitude(latB);
        locationB.setLongitude(longB);
        float distance = locationA.distanceTo(locationB)/1000;
        return distance;
    }
    private ProfileModel getPreferences(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        Gson gson = new Gson();
        String json = preferences.getString("myProfile", "");
        ProfileModel obj = gson.fromJson(json, ProfileModel.class);
//        Log.d("2111", "-----------------------------------------------" + obj.getUid());
        return obj;
    }

    public Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

//    private Bitmap retreiveImageFmServer(){
//        final long ONE_MEGABYTE = 1024 * 1024;
//
//        mStorage.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
//            @Override
//            public void onSuccess(byte[] bytes) {
//                bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
//
////                userPhotoList.add(bitmap);
//                // Data for "images/island.jpg" is returns, use this as needed
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception exception) {
//                // Handle any errors
//            }
//        });
//        return bitmap;
//    }

    private void initAdapter(final ArrayList<ProfileModel> userList){
        int i = 0;
        for (i = 0 ; i < (4) ; i++){
            devidedUserList.add(i, userList.get(i));
        }
        index = i;
//        Log.e("123","i is : " +index);
//        adapter = new GridAdapter(getContext(),devidedUserList);
        adapter = new GridAdapter(getContext(),devidedUserList);
        gridView.setAdapter(adapter);
//        myLastVisiblePos = gridView.getFirstVisiblePosition();
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }

            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                this.isScrollCompleted(absListView, scrollState);
                currentScrollState = scrollState;
            }
            private void isScrollCompleted(AbsListView absListView, int scrollState) {
                if (!absListView.canScrollVertically(1) && currentScrollState == SCROLL_STATE_IDLE) {
                    Log.e("123","scroll to end");
                    scrolltoEnd();
                }
            }

        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent toUserHome = new Intent(getContext(), UserHomeProfileActivity.class);
                toUserHome.putExtra("token",userList.get(position).getUid());
                startActivity(toUserHome);
            }
        });
    }

    private void scrolltoEnd(){
        int i = 0;
        for (i = index ; i < (index +4) ; i++){

            if (i <= (userList.size()-1)){
                devidedUserList.add(i, userList.get(i));
                if (i == (userList.size()-1)){
                    i++;
                    break;
                }
            }
        }
        index = i;
        Log.e("123","i is : " +i +", " + userList.size());
        if (index <= userList.size()){
            adapter.setUserList(devidedUserList);
            adapter.notifyDataSetChanged();
//            gridView.setAdapter(adapter);
        }
        if (index == (userList.size())){
            index = index +2;
        }
    }

    private String getUserToken() {
        String userUid = "";
        if (user != null) {
            userUid = user.getUid();
        }
        return userUid;
    }
}