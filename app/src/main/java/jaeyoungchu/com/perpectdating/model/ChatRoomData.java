package jaeyoungchu.com.perpectdating.model;

/**
 * Created by chu on 2016-10-29.
 */

public class ChatRoomData {
    private String userName,msg,userToken,opUserToken,opUserName,opPhotoUrl;
    private boolean right,read;
    private Long lastMsgTime;

    public ChatRoomData() {
        this.userName = "";
        this.msg = "";
        this.userToken = "";
        this.opUserToken = "";
        this.opUserName = "";
        this.opPhotoUrl = "";
        this.right = true;
        this.lastMsgTime = (long)0;
    }

    public ChatRoomData(String userName, String msg, String userToken, String opUserToken, String opUserName, String opPhotoUrl, boolean right,Long lastMsgTime,boolean read) {
        this.userName = userName;
        this.msg = msg;
        this.userToken = userToken;
        this.opUserToken = opUserToken;
        this.opUserName = opUserName;
        this.opPhotoUrl = opPhotoUrl;
        this.right = right;
        this.lastMsgTime = lastMsgTime;
        this.read = read;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public Long getLastMsgTime() {
        return lastMsgTime;
    }

    public void setLastMsgTime(Long lastMsgTime) {
        this.lastMsgTime = lastMsgTime;
    }

    public String getUserName() {
        return userName;
    }

    public String getMsg() {
        return msg;
    }

    public String getUserToken() {
        return userToken;
    }

    public String getOpUserToken() {
        return opUserToken;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public String getOpPhotoUrl() {
        return opPhotoUrl;
    }

    public boolean isRight() {
        return right;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public void setOpUserToken(String opUserToken) {
        this.opUserToken = opUserToken;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public void setOpPhotoUrl(String opPhotoUrl) {
        this.opPhotoUrl = opPhotoUrl;
    }

    public void setRight(boolean right) {
        this.right = right;
    }
}
