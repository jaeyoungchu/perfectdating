package jaeyoungchu.com.perpectdating.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by chu on 2016-10-23.
 */
@IgnoreExtraProperties
public class ProfileModel  {
    private String username;
    private String zender;
    private String uid;
    private int year;
    private int month;
    private int date;
    private String smallProfileUri;
//    private Bitmap profilePhotoImage;
    double latitude;
    double longitude;
    public String photoUrl;
    float distanceFromMe;

    public ProfileModel() {

    }

    public ProfileModel(String username, String zender, String uid, int year, int month, int date, double latitude, double longitude, String photoUrl,String smallUri) {
        this.username = username;
        this.zender = zender;
        this.uid = uid;
        this.year = year;
        this.month = month;
        this.date = date;
        this.latitude = latitude;
        this.longitude = longitude;
        this.photoUrl = photoUrl;
        this.smallProfileUri = smallUri;
//        this.profilePhotoImage = null;
    }

    public float getDistanceFromMe() {
        return distanceFromMe;
    }

    public void setDistanceFromMe(float distanceFromMe) {
        this.distanceFromMe = distanceFromMe;
    }

    public String getSmallProfileUri() {
        return smallProfileUri;
    }

    public void setSmallProfileUri(String smallProfileUri) {
        this.smallProfileUri = smallProfileUri;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setZender(String zender) {
        this.zender = zender;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUsername() {
        return username;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getDate() {
        return date;
    }

    public String getZender() {
        return zender;
    }

    public String getUid() {
        return uid;
    }

//    public Bitmap getProfilePhotoImage() {
//        return profilePhotoImage;
//    }
//
//    public void setProfilePhotoImage(Bitmap profilePhotoImage) {
//        this.profilePhotoImage = profilePhotoImage;
//    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
