package jaeyoungchu.com.perpectdating.model;

import com.google.firebase.database.ServerValue;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by chu on 2016-10-27.
 */

public class ChatData {
    private String userName,msg,userToken,opUserToken,opUserName,userPhotoUrl,opUserPhotoUrl;
    private boolean right,picture;
    public boolean read;
    public Long creationDate;

    public ChatData() {
        this.userName = "";
        this.msg = "";
        this.userToken = "";
        this.opUserToken = "";
        this.opUserName = "";
        this.userPhotoUrl = "";
        this.opUserPhotoUrl = "";
    }

    public ChatData(String userName,String opUserName, String msg, String userToken,String opUserToken, boolean right,boolean isRead,boolean picture,String userPhotoUrl,String opUserPhotoUrl) {
        this.userName = userName;
        this.opUserName = opUserName;
        this.msg = msg;
        this.userToken = userToken;
        this.opUserToken = opUserToken;
        this.right = right;
        this.read = isRead;
        this.picture = picture;
        this.userPhotoUrl = userPhotoUrl;
        this.opUserPhotoUrl = opUserPhotoUrl;
    }

    public boolean isPicture() {
        return picture;
    }

    public void setPicture(boolean picture) {
        this.picture = picture;
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }

    public void setOpUserPhotoUrl(String opUserPhotoUrl) {
        this.opUserPhotoUrl = opUserPhotoUrl;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public String getOpUserPhotoUrl() {
        return opUserPhotoUrl;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean isRead) {
        read = isRead;
    }

    public java.util.Map<String, String> getCreationDate() {
        return ServerValue.TIMESTAMP;
    }
    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }

    public String getOpUserName() {
        return opUserName;
    }

    public void setOpUserName(String opUserName) {
        this.opUserName = opUserName;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public String getOpUserToken() {
        return opUserToken;
    }

    public String getUserName() {
        return userName;
    }

    public String getMsg() {
        return msg;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public void setOpUserToken(String opUserToken) {
        this.opUserToken = opUserToken;
    }


//    @Override
//    public int compare(ChatData lhs, ChatData rhs) {
//
//        return Long.compare(lhs.creationDate, rhs.creationDate);
//
//    }
}
