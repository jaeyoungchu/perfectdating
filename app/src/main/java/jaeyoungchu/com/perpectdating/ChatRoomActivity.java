package jaeyoungchu.com.perpectdating;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import jaeyoungchu.com.perpectdating.adapter.ChatAdapter;
import jaeyoungchu.com.perpectdating.adapter.GridAdapter;
import jaeyoungchu.com.perpectdating.adapter.MsgListAdapter;
import jaeyoungchu.com.perpectdating.helper.ShakeDetector;
import jaeyoungchu.com.perpectdating.helper.ShakeService;
import jaeyoungchu.com.perpectdating.model.ChatData;
import jaeyoungchu.com.perpectdating.model.ProfileModel;
import jaeyoungchu.com.perpectdating.services.FirebaseBackgroundService;

import static android.graphics.Color.BLACK;
import static android.view.Gravity.LEFT;
import static android.view.Gravity.RIGHT;

/**
 * Created by chu on 2016-10-28.
 */

public class ChatRoomActivity extends AppCompatActivity implements View.OnClickListener {
    private StorageReference mStorage;
    final int RESULT_LOAD_IMAGE = 4;
    public static boolean inRoom=false;
    public static String opUserTokenInNotification = "dummy token";
    private SensorManager mSensorManager;
    private Uri destination;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    private Button sendMsgBtn,exitBtn,deleteBtn;
    private Button pictureBtn;
    private EditText msgET;
    private Bitmap profilePhoto;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private String opUserName,myUserName;
    private MsgListAdapter adapter;
    private DatabaseReference myDBRef;
    private ProfileModel opProfile,myProfile,receivedProfile;
    private String userPhotoUrl,opUserPhotoUrl;
    private String opToken,deviceToken,profileUrl;
    FirebaseUser user;
    private ListView listView;
    private LinearLayout mlayout,mlayout1;
    private TextView chatMsgTV;
    FirebaseListAdapter<ChatData> firebaseListAdapter;
    private Query queryRef;
    private ChatAdapter chatAdapter;
    private com.google.firebase.database.ChildEventListener readListener;
    private ArrayList<ChatData> chatDataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatroom);
        inRoom = true;
        chatDataList = new ArrayList<ChatData>();
        myDBRef = FirebaseDatabase.getInstance().getReferenceFromUrl("https://perpectdating.firebaseio.com/");

        listView = (ListView)findViewById(R.id.chatRoomListView);
        user = FirebaseAuth.getInstance().getCurrentUser();
        exitBtn = (Button) findViewById(R.id.chatroom_exit_button);
        deleteBtn = (Button) findViewById(R.id.chat_room_delete_button);
        sendMsgBtn =(Button) findViewById(R.id.send_msg_button);
        pictureBtn = (Button)findViewById(R.id.picture_button);
        msgET = (EditText) findViewById(R.id.chat_msg_ET);
        msgET.clearFocus();

        opToken = getIntent().getExtras().getString("opToken");
        opProfile = new ProfileModel();
        myProfile = new ProfileModel();
        opProfile = getSelectedUserObj(opToken);

        myProfile = getSelectedUserObj(getUserToken());
        deviceToken = getUserToken();
        mStorage = FirebaseStorage.getInstance().getReference().child("chatroom_photo").child(deviceToken);



        Intent intent = new Intent(ChatRoomActivity.this, ShakeService.class);
        startService(intent);

        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {
            @Override
            public void onShake(int count) {
                promptSpeechInput();
            }
        });

        pictureBtn.setOnClickListener(this);
//        pictureBtn.setVisibility(View.GONE);
        sendMsgBtn.setOnClickListener(this);
        exitBtn.setOnClickListener(this);
        deleteBtn.setOnClickListener(this);
        connectChatDB();
    }
    private void initAdapter(){
        chatAdapter = new ChatAdapter(getBaseContext(),chatDataList,deviceToken,profilePhoto);
        listView.setDivider(null);
        listView.setDividerHeight(0);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                final ImageView imageView = (ImageView) view.findViewById(R.id.picture_imageview);
                if (imageView.getDrawable() != null){

                    ChatData item = (ChatData)adapterView.getItemAtPosition(i);
                    String ImagefileName = item.getMsg().substring(87,item.getMsg().length());
                    String clickedImageUri = Environment.getExternalStorageDirectory() + "/perfectDatingProfile/chatImage/"+ImagefileName+".jpg";

                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(clickedImageUri)), "image/*");
                    startActivity(intent);
                }
            }
        });
        listView.setAdapter(chatAdapter);
    }

    private void connectChatDB(){
        myDBRef.child("chatroom").child(deviceToken).child(opToken).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ChatData cData = dataSnapshot.getValue(ChatData.class);
                chatDataList.add(cData);
//                Log.d("211","-------- datasnapshot : " +cData.getMsg());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d("inroom : ", "  " + inRoom);
                ChatData cData = dataSnapshot.getValue(ChatData.class);
                for (int i = 0 ; i < chatDataList.size() ; i++){
                    if(chatDataList.get(i).getMsg().equals(cData.getMsg())){
                        chatDataList.set(i,cData);
                Log.d("123123","-------- datasnapshot : " +cData.getMsg());
                Log.d("123123","-------- is read : " +cData.isRead());
                    }
                }
                chatAdapter.notifyDataSetChanged();
                listView.setAdapter(chatAdapter);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        initAdapter();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.picture_button:
                choosePicture();
                break;
            case R.id.send_msg_button:
                String msg = msgET.getText().toString();
                if (msg.isEmpty()){
                    Toast.makeText(getApplicationContext(),"input some thing...",Toast.LENGTH_LONG).show();
                    return;
                }
                sendMsg(msg);
                msgET.setText("");
                break;
            case R.id.chatroom_exit_button:
                onBackPressed();
                break;
            case R.id.chat_room_delete_button:
                new AlertDialog.Builder(this)
                        .setTitle("Close room")
                        .setMessage("If you close room, You can not read all chats in this room.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteChat();
                                onBackPressed();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                break;
        }
    }

    private void choosePicture(){
        Intent cropIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        cropIntent.putExtra("crop","true");
        cropIntent.putExtra("return-data",false);
        File direct = new File(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/chatCropImage/");
        if (!direct.exists()) {
            File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/chatCropImage/");
            wallpaperDirectory.mkdirs();
        }
        destination = Uri.fromFile(new File(Environment.getExternalStorageDirectory()+"/perfectDatingProfile/chatCropImage/", "cropped.png"));
        cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, destination);
        startActivityForResult(cropIntent, RESULT_LOAD_IMAGE);
    }
//    private void choosePicture1(){
//        Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(i, RESULT_LOAD_IMAGE);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    sendMsg(result.get(0));
                }
                break;
            }
            case RESULT_LOAD_IMAGE: {
                if (resultCode == RESULT_OK) {
                    File imageFile = new File(destination.getPath());
                    Log.d("123","------------------------------------0-00-0-"+destination);
                    Bitmap resizedBitmap = decodeFile(destination.getPath(),880);


                    uploadPicture(Uri.fromFile(bitmapToFile(resizedBitmap)));
//                    uploadPicture(Uri.fromFile(imageFile));

                }
                break;
            }
//            case 1234:{
//                chatAdapter.onActivityResult(requestCode, resultCode, data);
//            }
        }
    }
    public static Bitmap decodeFile(String filePath, final int REQUIRED_SIZE) {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp <= REQUIRED_SIZE && height_tmp <= REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, o2);
        return bitmap;
    }
    private File bitmapToFile(Bitmap bitmap) {


        File smallImageFile = new File(Environment.getExternalStorageDirectory()+"/perfectDatingProfile/chatCropImage/", "cropped.png");

        OutputStream os;
        try {
            os = new FileOutputStream(smallImageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return smallImageFile;
    }
    private void uploadPicture(Uri selectedPictureUri){

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new java.util.Date());
        StorageReference filepath = mStorage.child("code"+currentDateandTime);

        filepath.putFile(selectedPictureUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUri = taskSnapshot.getDownloadUrl();
                sendPicture(downloadUri.toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(ChatRoomActivity.this,"Upload Failed..",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void deleteChat(){
        myDBRef.child("chatroom").child(deviceToken).child(opToken).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().removeValue();
//                    Log.d("211","-------- datasnapshot : " +msgSnapshot.toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



    private void setReadTrue(){
        queryRef = myDBRef.child("chatroom").child(opToken).child(deviceToken).orderByChild("read").equalTo(true);
        readListener = queryRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(final DataSnapshot snapshot, String previousChild) {
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    if (postSnapshot.getKey().equals("read")){
                        myDBRef.child("chatroom").child(opToken).child(deviceToken).child(snapshot.getKey()).child("read").setValue(false);
                    }
                }
                chatAdapter.notifyDataSetChanged();
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }



    private ProfileModel getSelectedUserObj(final String token){
        myDBRef.child("profile").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                receivedProfile = dataSnapshot.child(token).getValue(ProfileModel.class);
                myUserName = receivedProfile.getUsername();
                userPhotoUrl = receivedProfile.getSmallProfileUri();
                if(token.equals(opToken)){
                    exitBtn.setText("< " + receivedProfile.getUsername());
                    profileUrl = receivedProfile.getSmallProfileUri();
                    opUserName = receivedProfile.getUsername();
                    Log.d("-----",profileUrl.toString());
                    if (profileUrl.equals("none")){
                        Log.d("-----",profileUrl.toString());
                        profilePhoto = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.icon250);
                        chatAdapter.setProfilePhoto(profilePhoto);
                    }else {
                        profilePhoto = getBitmapFromURL(profileUrl);
                        chatAdapter.setProfilePhoto(profilePhoto);
                    }
                    opUserPhotoUrl = receivedProfile.getSmallProfileUri();
                }
//                Log.d("-------",receivedProfile.getUsername());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("----", "Failed to read value.", databaseError.toException());
            }
        });

        return receivedProfile;
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));
        Log.d("2111" , "in pormpt speech input method---------------");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     * */


    private void sendPicture(String url){
        myDBRef.child("chatroom").child(deviceToken).child(opToken).push().setValue(new ChatData(myUserName,opUserName,url,deviceToken,opToken,true,true,true,userPhotoUrl,opUserPhotoUrl));

        myDBRef.child("chatroom").child(opToken).child(deviceToken).push().setValue(new ChatData(myUserName,opUserName,url,deviceToken,opToken,true,true,true,userPhotoUrl,opUserPhotoUrl));
    }


    private void sendMsg(String msg){
        myDBRef.child("chatroom").child(deviceToken).child(opToken).push().setValue(new ChatData(myUserName,opUserName,msg,deviceToken,opToken,true,true,false,userPhotoUrl,opUserPhotoUrl));

        myDBRef.child("chatroom").child(opToken).child(deviceToken).push().setValue(new ChatData(myUserName,opUserName,msg,deviceToken,opToken,true,true,false,userPhotoUrl,opUserPhotoUrl));
    }


    public Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }
    public void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }
    @Override
    protected void onStart() {
        super.onStart();
        setReadTrue();

        mSensorManager.registerListener(mShakeDetector, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        Log.d("2133-----","on start Chatroom Activicy-------------------");
    }

    @Override
    protected void onResume() {
        super.onResume();
        opUserTokenInNotification = deviceToken;
        Log.d("2133-----","is exist service ? 2.5 " + isMyServiceRunning(FirebaseBackgroundService.class));
        stopService(new Intent(getApplicationContext(), FirebaseBackgroundService.class));
        Log.d("2133-----","is exist service ? 3 " + isMyServiceRunning(FirebaseBackgroundService.class));
        Log.d("2133-----","on resume Chatroom Activicy ------------------- ");

    }

//    @Override
//    protected void onPause() {
//        Log.d("2133-----","on pause Chatroom Activicy -------------------");
//        super.onPause();
//
//    }

    @Override
    protected void onStop() {
        Log.d("2133-----","on stop Chatroom Activicy -------------------");
        super.onStop();
        inRoom = false;
        mSensorManager.unregisterListener(mShakeDetector);
//        Log.e("excuted methoud", "remove listener()");
        queryRef.removeEventListener(readListener);
        Log.d("2133-----","is exist service ? 4 " + isMyServiceRunning(FirebaseBackgroundService.class));
        startService(new Intent(getApplicationContext(), FirebaseBackgroundService.class));
        Log.d("2133-----","is exist service ? 5 " + isMyServiceRunning(FirebaseBackgroundService.class));
    }

    @Override
    protected void onDestroy() {
        Log.d("2133-----","on destory Chatroom Activicy -------------------");

        super.onDestroy();
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    private String getUserToken(){
        if (user != null) {
            return user.getUid();
//            Log.d(TAG,"user Token " + userUid);
        }else {
            return null;
        }
    }


}
