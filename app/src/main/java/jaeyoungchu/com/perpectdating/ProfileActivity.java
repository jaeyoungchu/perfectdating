package jaeyoungchu.com.perpectdating;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Calendar;


import jaeyoungchu.com.perpectdating.model.ProfileModel;

/**
 * Created by chu on 2016-10-23.
 */

public class ProfileActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener{

//    private DatabaseReference myRef;
    private FirebaseUser user;

    private final String TAG = "L Profile Activity Log";
    EditText userNameET;
    RadioGroup sexRadioGroup;
    RadioButton maleRadioButton, femaleRadioButton;
    Button saveProfileButton;
    String sex;

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager locationManager;
    private LocationRequest mLocationRequest;
    double latitude = 0;
    double longitude = 0;
    private DatePicker mDatePicker;
    private TextView tv;
    int year;
    int month;
    int dayOfMonth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        userNameET = (EditText)findViewById(R.id.userNameEditText);
        sexRadioGroup = (RadioGroup)findViewById(R.id.sexRadioGroup);
        maleRadioButton= (RadioButton)findViewById(R.id.male_radio_button);
        femaleRadioButton= (RadioButton)findViewById(R.id.female_radio_button);
        saveProfileButton = (Button)findViewById(R.id.profile_save_button);

        final Calendar c= Calendar.getInstance();
        year = c.get(c.YEAR);
        month = c.get(c.MONTH);
        dayOfMonth = c.get(c.DAY_OF_MONTH);
        mDatePicker = (DatePicker)findViewById(R.id.datePicker);
        tv = (TextView)findViewById(R.id.datePickerTV);
        mDatePicker.init(1986, mDatePicker.getMonth(), mDatePicker.getDayOfMonth(),new DatePicker.OnDateChangedListener() {

            @Override
            public void onDateChanged(DatePicker arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
                tv.setText(" "+arg3+ " / "+ (arg2+1) + " / "+arg1);
                year = arg1;
                month = arg2 +1;
                dayOfMonth = arg3;
            }
        } );

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);

//        myRef = FirebaseDatabase.getInstance().getReference();
        user = FirebaseAuth.getInstance().getCurrentUser();
        sexRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.male_radio_button:
                        sex = "male";
                        break;
                    case R.id.female_radio_button:
                        sex= "female";
                        break;
                }
            }
        });

        saveProfileButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String userName = userNameET.getText().toString();
                String uid ="";
                if (TextUtils.isEmpty(userName)) {
                    userNameET.setError("Required User Name.");
                }else if(sexRadioGroup.getCheckedRadioButtonId() == -1){
                    Toast.makeText(ProfileActivity.this, "Check your Gender",
                            Toast.LENGTH_LONG).show();
                }else{
                    if (user != null) {
                        uid = user.getUid();

                    }else{
                        Toast.makeText(ProfileActivity.this, "invalid user uid",
                                Toast.LENGTH_LONG).show();
                    }
                    Log.d(TAG,"uid : " + uid +" year : " + year + " month : " + month + " date :" + dayOfMonth + " longitude : " + longitude + " latitude : " + latitude);
                    Intent toPhotoActivity = new Intent(getApplicationContext(),PhotoActivity.class);

                    toPhotoActivity.putExtra("userName",userName);
                    toPhotoActivity.putExtra("sex",sex);
                    toPhotoActivity.putExtra("uid",uid);
                    toPhotoActivity.putExtra("year",year);
                    toPhotoActivity.putExtra("month",month);
                    toPhotoActivity.putExtra("dayOfMonth",dayOfMonth);
                    toPhotoActivity.putExtra("latitude",latitude);
                    toPhotoActivity.putExtra("longitude",longitude);

                    Log.d("2133","--------------------------");

                    startActivity(toPhotoActivity);
                }

            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {
            latitude = mLocation.getLatitude();
            longitude = mLocation.getLongitude();
            Log.d("============>>>>>","latitude : " + latitude + ", longitude : " + longitude);
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
        Log.d("============>>>>>","latitude : " + latitude + ", longitude : " + longitude);
    }

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(900000)
                .setFastestInterval(70000);
        // Request location updates

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            Log.d("TAG","need method to ask permission");

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
    @Override
    public void onLocationChanged(Location location) {

    }
}
