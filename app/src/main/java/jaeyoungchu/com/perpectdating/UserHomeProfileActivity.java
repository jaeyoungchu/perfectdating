package jaeyoungchu.com.perpectdating;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import jaeyoungchu.com.perpectdating.model.ProfileModel;

/**
 * Created by chu on 2016-10-27.
 */

public class UserHomeProfileActivity extends AppCompatActivity {

    private ImageView homeMainIV;
    private Button startChatBtn;
    private Uri downloadUri;
    private String token;
    private StorageReference islandRef;
    private StorageReference mStorage;
    private ProfileModel receivedProfile;
    private DatabaseReference myDBRef;
    private FirebaseDatabase database;
    private TextView userName;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userhomeprofile);
        database = FirebaseDatabase.getInstance();
        myDBRef = database.getReference("profile");

        token = getIntent().getExtras().getString("token");
        mStorage = FirebaseStorage.getInstance().getReference().child("profile_photo").child(token);
        getSelectedUserObj();
        homeMainIV = (ImageView)findViewById(R.id.userHomeMainIV);
        startChatBtn = (Button)findViewById(R.id.start_chat_button);
        userName = (TextView)findViewById(R.id.userHomeMainNameTV);

        startChatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toChatRoom = new Intent(UserHomeProfileActivity.this,ChatRoomActivity.class);
                toChatRoom.putExtra("opToken",token);
                startActivity(toChatRoom);
            }
        });

    }

    private void getSelectedUserObj(){
        myDBRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                receivedProfile = dataSnapshot.child(token).getValue(ProfileModel.class);
//                String value = dataSnapshot.child(uid).getValue(String.class);
                if(receivedProfile == null){
                    Log.d("----", "no value and first time log in");
                    startActivity(new Intent(getBaseContext(),ProfileActivity.class));
                }else{
//                    Log.d("2222", "photo url : " + receivedProfile.getPhotoUrl());
                    if (receivedProfile.getPhotoUrl().equals("none")){
                        homeMainIV.setImageResource(R.drawable.icon250);
                    }else {
                        homeMainIV.setImageBitmap(getBitmapFromURL(receivedProfile.getPhotoUrl()));
                    }

                    userName.setText(receivedProfile.getUsername());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("----", "Failed to read value.", databaseError.toException());
            }
        });
    }



    public Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

}
