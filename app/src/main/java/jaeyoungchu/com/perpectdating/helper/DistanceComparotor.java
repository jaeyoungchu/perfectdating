package jaeyoungchu.com.perpectdating.helper;

import java.util.Comparator;

import jaeyoungchu.com.perpectdating.model.ProfileModel;

/**
 * Created by chu on 2016-11-03.
 */

public class DistanceComparotor implements Comparator<ProfileModel> {

    @Override
    public int compare(ProfileModel o1, ProfileModel o2) {
        float change1 = o1.getDistanceFromMe();
        float change2 = o2.getDistanceFromMe();
        if (change1 < change2) return -1;
        if (change1 > change2) return 1;
        return 0;
    }
}
