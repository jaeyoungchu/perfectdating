package jaeyoungchu.com.perpectdating.helper;

import java.util.Comparator;

import jaeyoungchu.com.perpectdating.model.ChatRoomData;

/**
 * Created by chu on 2016-10-31.
 */

public class LastMsgTimeComparator implements Comparator<ChatRoomData> {
    @Override
    public int compare(ChatRoomData o1, ChatRoomData o2) {
        return o2.getLastMsgTime().compareTo(o1.getLastMsgTime());
    }
}