package jaeyoungchu.com.perpectdating.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import jaeyoungchu.com.perpectdating.BaseActivity;
import jaeyoungchu.com.perpectdating.ChatRoomActivity;
import jaeyoungchu.com.perpectdating.R;
import jaeyoungchu.com.perpectdating.model.ChatData;
import jaeyoungchu.com.perpectdating.model.ProfileModel;


import static jaeyoungchu.com.perpectdating.ChatRoomActivity.inRoom;

/**
 * Created by Administrator on 2017-02-08.
 */

public class FirebaseBackgroundService extends Service {
    private DatabaseReference myDBRef;
    FirebaseUser user;
    private String userUid,userName;
    private String previosMsg;
    private ProfileModel receivedProfile;
    private com.google.firebase.database.ChildEventListener readListener,secondReadListener;
    private Query queryRef;
    private Boolean isInRoom;
    Uri soundUri;
    Intent intent;
    PendingIntent pendingIntent;
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        cancelNotification(getBaseContext(),001);
        previosMsg = "dummy";
        inRoom = false;
        soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        intent = new Intent(this, BaseActivity.class);
        pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        try {
            myDBRef = FirebaseDatabase.getInstance().getReferenceFromUrl("https://perpectdating.firebaseio.com/");
        }catch (Exception e){
            FirebaseApp.initializeApp(getApplicationContext(), FirebaseOptions.fromResource(getApplicationContext()));
            myDBRef = FirebaseDatabase.getInstance().getReferenceFromUrl("https://perpectdating.firebaseio.com/");
        }

        user = FirebaseAuth.getInstance().getCurrentUser();

        getUserToken();
        getUserInfo();

    }



    private void getUserToken(){
        if (user != null) {
            userUid = user.getUid();
//            Log.d(TAG,"user Token " + userUid);
        }
    }

    private void getUserInfo(){
        myDBRef.child("profile").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                receivedProfile = dataSnapshot.child(userUid).getValue(ProfileModel.class);
                userName = receivedProfile.getUsername();
                activateListner();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("11", "Failed to read value.", databaseError.toException());
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (readListener != null){
            queryRef.removeEventListener(readListener);
        }

        intent = null;
        pendingIntent = null;
        inRoom =true;

    }


    private void activateListner(){

            queryRef = myDBRef.child("chatroom").child(userUid);

            readListener = queryRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(final DataSnapshot snapshot, String previousChild) {

                        secondReadListener = myDBRef.child("chatroom").child(snapshot.getKey()).child(userUid).orderByChild("creationDate").limitToLast(1).addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                                ChatData cData = dataSnapshot.getValue(ChatData.class);
                                if (cData.isRead() == true) {

                                    if (!cData.getUserName().equals(userName) && !inRoom) {
                                        if (cData.isPicture()) {
                                            pushNotification(cData.getOpUserName(), "picture");
                                        } else {
                                            pushNotification(cData.getOpUserName(), cData.getMsg());
                                        }
                                    }
                                }
                                myDBRef.child("chatroom").child(snapshot.getKey()).child(userUid).removeEventListener(secondReadListener);
                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {
                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                }
                @Override
                public void onChildChanged(final DataSnapshot dataSnapshot2, String s) {
                }
                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }
                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
    }

    private void pushNotification(String userName, String msg){


        if (!previosMsg.equals(msg)){
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.ic_action_new_msg)
                            .setContentTitle(userName)
                            .setContentText(msg)
                            .setContentIntent(pendingIntent)
                            .setAutoCancel(true)
                            .setSound(soundUri);
            int mNotificationId = 001;
            mBuilder.setVibrate(new long[] { 0, 1000, 1000 });
            mBuilder.setLights(Color.RED, 3000, 3000);
            NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotifyMgr.notify(mNotificationId, mBuilder.build());
            previosMsg = msg;
        }
    }

    public void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }

}