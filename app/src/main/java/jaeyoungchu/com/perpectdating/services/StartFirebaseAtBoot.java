package jaeyoungchu.com.perpectdating.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Administrator on 2017-02-08.
 */

public class StartFirebaseAtBoot extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, FirebaseBackgroundService.class));
    }
}
