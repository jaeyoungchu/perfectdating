package jaeyoungchu.com.perpectdating.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import jaeyoungchu.com.perpectdating.R;
import jaeyoungchu.com.perpectdating.model.ChatData;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.GRAY;
import static android.graphics.Color.HSVToColor;
import static android.graphics.Color.YELLOW;
import static android.graphics.Color.alpha;
import static android.view.Gravity.LEFT;
import static android.view.Gravity.RIGHT;

/**
 * Created by chu on 2016-10-28.
 */

public class MsgListAdapter extends BaseAdapter {

    private LinearLayout mlayout,mlayout1;
    private ArrayList<ChatData> chatList;
    private TextView chatMsgTV;
    private Context context;
    private LayoutInflater inflater;

    public MsgListAdapter(Context context, ArrayList<ChatData> chatList) {
        this.context = context;
        this.chatList = chatList;

    }

    @Override
    public int getCount() {
        return chatList.size();
    }

    @Override
    public Object getItem(int position) {
        return chatList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public ArrayList<ChatData> getChatList() {
        return chatList;
    }

    public void setChatList(ArrayList<ChatData> chatList) {
        this.chatList = chatList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listViewG = convertView;

        if(convertView == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            listViewG = inflater.inflate(R.layout.listview_custom_layout,null);
        }
        mlayout = (LinearLayout)listViewG.findViewById(R.id.msg_linear);
        mlayout1 = (LinearLayout)listViewG.findViewById(R.id.msg_linear1);
        chatMsgTV = (TextView)listViewG.findViewById(R.id.chat_msg_tv);

        chatMsgTV.setTextColor(BLACK);
        chatMsgTV.setText(chatList.get(position).getMsg());
//        Log.d("0=====","" +chatList.get(position).getDeviceToken() +",    " + chatList.get(position).getUserToken());
        if (chatList.get(position).isRight()){
//        if (true){
            Log.d("compare---"," is true -----------");
            mlayout.setGravity(RIGHT);
            mlayout.setPadding(300,5,10,5);
            mlayout1.setBackgroundColor(Color.parseColor("#ffc266"));
//            mlayout1.setBackgroundResource(R.drawable.bubbleb);
        }else {
            Log.d("compare---"," is ----------- false");
            mlayout.setGravity(LEFT);
            mlayout.setPadding(10,5,300,5);
            mlayout1.setBackgroundColor(Color.parseColor("#dfff80"));
//            mlayout1.setBackgroundResource(R.drawable.bubblea);
        }
//        nameTV.setText(userList.get(position).getUsername());
//        ageTV.setText(userList.get(position).getZender());
//        gridViewProfileIV.setImageBitmap(userList.get(position).getProfilePhotoImage());
        return listViewG;
    }
}
