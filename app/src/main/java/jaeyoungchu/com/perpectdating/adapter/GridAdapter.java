package jaeyoungchu.com.perpectdating.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import jaeyoungchu.com.perpectdating.R;
import jaeyoungchu.com.perpectdating.model.ProfileModel;

/**
 * Created by chu on 2016-10-24.
 */

public class GridAdapter extends BaseAdapter {

    private ArrayList<ProfileModel> userList;

    private Context context;
    private LayoutInflater inflater;

    public GridAdapter(Context context, ArrayList<ProfileModel> userList) {
        this.userList = userList;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    private static class ViewHolder {
        Bitmap bitmap;
        ImageView gridViewProfileIV;
        String imageURL;
        ProgressBar mpBar;
        TextView nameTV;
        TextView distanceTV;
        float nNumber;
        String strNumber,userName;
    }
    public void setUserList(ArrayList<ProfileModel> userList){
        this.userList = userList;
    }


    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
//        Log.d("123","   "+position);
//        if (position < userList.size())
//            return 0;
//        return 1;
     return position;

    }
    @Override
    public int getViewTypeCount() {
        return 500;
//        return getCount();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if(convertView == null){
//            Log.d("123","---------------- user name : " + userList.get(position).getUsername() + ", position : " + position);
//            convertView = inflater.inflate(R.layout.gridview_custom_layout,null);
//            Log.d("123","---------------- if position : " + position);
            inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(R.layout.gridview_custom_layout, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.nameTV = (TextView)convertView.findViewById(R.id.gridViewNameTV);
            viewHolder.distanceTV = (TextView)convertView.findViewById(R.id.gridViewAgeTV);
            viewHolder.gridViewProfileIV = (ImageView)convertView.findViewById(R.id.gridViewIV);
            viewHolder.mpBar = (ProgressBar)convertView.findViewById(R.id.progressBar);
            viewHolder.strNumber = String.format("%.2f", userList.get(position).getDistanceFromMe());
            viewHolder.userName = userList.get(position).getUsername();
            viewHolder.imageURL = userList.get(position).getPhotoUrl().toString();

            viewHolder.nameTV.setText( viewHolder.userName );
            viewHolder.distanceTV.setText( viewHolder.strNumber +"km");
            new DownloadAsyncTask().execute(viewHolder);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();


        }
        return convertView;
    }

    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {

        @Override
        protected ViewHolder doInBackground(ViewHolder... params) {
            ViewHolder viewHolder = params[0];
            try {
                if (viewHolder.imageURL.equals("none")){

                }else{
                    Log.d("check latancy ","doIn back ground");
                    URL imageURL = new URL(viewHolder.imageURL);
                    viewHolder.bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
                    viewHolder.bitmap = BitmapFactory.decodeStream(imageURL.openStream());
                }

            } catch (IOException e) {
                Log.e("error", "Downloading Image Failed");
                viewHolder.bitmap = null;
            }
            return viewHolder;
        }

        @Override
        protected void onPostExecute(ViewHolder result) {
            Log.d("check latancy ","onPostExecute()");
            if (result.bitmap == null) {
                result.mpBar.setVisibility(View.GONE);
                result.gridViewProfileIV.setBackgroundColor(Color.WHITE);
                result.gridViewProfileIV.setImageResource(R.drawable.icon250);

            } else {
                result.gridViewProfileIV.setImageBitmap(result.bitmap);
                result.mpBar.setVisibility(View.GONE);


            }
        }
    }


}
