package jaeyoungchu.com.perpectdating.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import jaeyoungchu.com.perpectdating.BaseActivity;
import jaeyoungchu.com.perpectdating.PhotoActivity;
import jaeyoungchu.com.perpectdating.R;
import jaeyoungchu.com.perpectdating.model.ChatData;


import static android.graphics.Color.BLACK;
import static android.view.Gravity.LEFT;
import static android.view.Gravity.RIGHT;



public class ChatAdapter extends BaseAdapter {
    private ArrayList<ChatData> chatDataList;
    private String deviceToken;
    private Context context;
    private LayoutInflater inflater;
    private Bitmap profilePhoto;

    private static class ViewHolder {
        Bitmap bitmap;
        ImageView pictureIV;
        String imageURL;
        LinearLayout mlayout,mlayout1;
        TextView chatMsgTV;
        ImageView profileIV;
        TextView isReadTV;
        ChatData cData;
        String clickedImageUri;
    }

    public ChatAdapter(Context context, ArrayList<ChatData> chatDataList,String deviceToken,Bitmap profilePhoto) {
        this.chatDataList = chatDataList;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.deviceToken = deviceToken;
        this.profilePhoto = profilePhoto;
    }

    @Override
    public View getView(int i, View v, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (v == null){
            inflater = LayoutInflater.from(this.context);
            v = inflater.inflate(R.layout.listview_custom_layout, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.mlayout = (LinearLayout) v.findViewById(R.id.msg_linear);
            viewHolder.mlayout1 = (LinearLayout) v.findViewById(R.id.msg_linear1);
            viewHolder.pictureIV = (ImageView) v.findViewById(R.id.picture_imageview);
            viewHolder.chatMsgTV = (TextView) v.findViewById(R.id.chat_msg_tv);
            viewHolder.profileIV = (ImageView) v.findViewById(R.id.small_profile_IV);
            viewHolder.isReadTV = (TextView) v.findViewById(R.id.isRead_TV);
            viewHolder.imageURL = chatDataList.get(i).getMsg();
            viewHolder.cData = chatDataList.get(i);
//            Log.d("2112","------------------------------ device token : " + deviceToken +" , getuser token : " + viewHolder.cData.getUserToken());
            if (viewHolder.cData.getUserToken().equals(deviceToken)) {
                viewHolder.cData.setRight(true);
            } else {
                viewHolder.cData.setRight(false);
            }

            viewHolder.chatMsgTV.setTextColor(BLACK);
            viewHolder.chatMsgTV.setText(viewHolder.cData.getMsg());

            if (viewHolder.cData.isRight()) {
//                    isReadTV.setVisibility(View.INVISIBLE);
                viewHolder.profileIV.setVisibility(View.GONE);
                viewHolder.mlayout.setGravity(RIGHT);
                viewHolder.mlayout.setPadding(300, 5, 10, 5);
                viewHolder.mlayout1.setBackgroundColor(Color.parseColor("#ffc266"));
            } else {

//                Log.d("2112","set invisible new text------------------------------" + profilePhoto.getHeight());
                viewHolder.cData.setRead(false);
                viewHolder.isReadTV.setVisibility(View.INVISIBLE);
                viewHolder.profileIV.setVisibility(View.VISIBLE);
                viewHolder.profileIV.setImageBitmap(profilePhoto);
                viewHolder.mlayout.setGravity(LEFT);
                viewHolder.mlayout.setPadding(10, 5, 300, 5);
                viewHolder.mlayout1.setBackgroundColor(Color.parseColor("#dfff80"));
            }

            if (viewHolder.cData.isPicture()){
                viewHolder.chatMsgTV.setVisibility(View.GONE);
            }else {
                viewHolder.pictureIV.setVisibility(View.GONE);
            }

            if (viewHolder.cData.isRead()) {
                viewHolder.isReadTV.setVisibility(View.VISIBLE);
            }else {
                viewHolder.isReadTV.setVisibility(View.INVISIBLE);
            }
            v.setTag(viewHolder);
            new DownloadAsyncTask().execute(viewHolder);
        }else {
            viewHolder = (ViewHolder)v.getTag();
        }
//        Log.e("123123","is read : " + viewHolder.cData.isRead()+ " , msg : " +viewHolder.cData.getMsg());
        if (viewHolder.cData.isRead()) {
            viewHolder.isReadTV.setVisibility(View.VISIBLE);
        }else {
            viewHolder.isReadTV.setVisibility(View.INVISIBLE);
        }
        v.setTag(viewHolder);
        return v;
    }

    public void setProfilePhoto(Bitmap profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public void setChatDataList(ArrayList<ChatData> chatDataList) {
        this.chatDataList = chatDataList;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 500;
    }

    @Override
    public int getCount() {
        return chatDataList.size();
    }

    @Override
    public Object getItem(int i) {
        return chatDataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {
        String imageName;
        Bitmap chatImage;

        @Override
        protected ViewHolder doInBackground(ViewHolder... params) {
            ViewHolder viewHolder = params[0];

            try {
                URL imageURL = new URL(viewHolder.imageURL);
                if (imageURL.toString().length() > 18){
                    imageName = imageURL.toString().substring(87,imageURL.toString().length());
//                    Log.d("--=-=-=",imageName);
                    if (checkExistanceOfFile(imageName)){
                        Log.d("=======","not null");
                        viewHolder.bitmap = bringImageFmLocal(imageName);
                        viewHolder.clickedImageUri =  Environment.getExternalStorageDirectory() + "/perfectDatingProfile/chatImage/"+imageName+".jpg";
                    }else {
                        Log.d("=======","null");
                        chatImage = BitmapFactory.decodeStream(imageURL.openStream());
                        createDirectoryAndSaveFile(chatImage,imageName);
                        viewHolder.bitmap = chatImage;
                    }
                }
//                viewHolder.bitmap = BitmapFactory.decodeStream(imageURL.openStream());
            } catch (IOException e) {
//                Log.e("error", "Downloading Image Failed" +e);
                viewHolder.bitmap = null;
            }
            return viewHolder;
        }

        @Override
        protected void onPostExecute(final ViewHolder result) {
            if (result.bitmap == null) {
                result.chatMsgTV.setTextColor(BLACK);
                result.chatMsgTV.setText(result.cData.getMsg());

            } else {

                result.pictureIV.setImageBitmap(result.bitmap);
            }
        }
        private void createDirectoryAndSaveFile(Bitmap imageToSave, String fileName) {
            File direct = new File(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/chatImage/");

            if (!direct.exists()) {
//            File wallpaperDirectory = new File("/sdcard/perfectDatingProfile/");
                File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/chatImage/");
                wallpaperDirectory.mkdirs();
            }

//        File file = new File(new File("/sdcard/perfectDatingProfile/"), fileName);
            File file = new File(new File(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/chatImage/"), fileName +".jpg");
            if (file.exists()) {
                file.delete();
            }
            try {
                FileOutputStream out = new FileOutputStream(file);
                imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        private boolean checkExistanceOfFile(String imageName){
            String path = Environment.getExternalStorageDirectory() + "/perfectDatingProfile/chatImage/"+imageName+".jpg";
            File file = new File(path);
            if(file.exists()){
                return true;
            }else {
                return false;
            }

        }
        private Bitmap bringImageFmLocal(String imagePath){
            Log.d("=======","bringImageFmLocal");
            Bitmap bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/chatImage/"+imagePath+".jpg");
            return bitmap;

        }
    }
}
