package jaeyoungchu.com.perpectdating.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import jaeyoungchu.com.perpectdating.R;
import jaeyoungchu.com.perpectdating.model.ChatRoomData;


/**
 * Created by chu on 2016-10-29.
 */

public class RoomListViewAdapter extends BaseAdapter {

    private ArrayList<ChatRoomData> RoomList;

    private Context context;
    private LayoutInflater inflater;

    public RoomListViewAdapter(Context context, ArrayList<ChatRoomData> RoomList) {
        this.RoomList = RoomList;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private static class ViewHolder {
        Bitmap bitmap;
        ImageView gridViewProfileIV;
        String imageURL;
    }


    @Override
    public int getCount() {
        return RoomList.size();
    }

    @Override
    public Object getItem(int position) {
        return RoomList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.room_listview_custom_layout, null);
            viewHolder = new ViewHolder();

            TextView nameTV = (TextView) convertView.findViewById(R.id.chat_room_opName_tv);
            TextView msgTV = (TextView) convertView.findViewById(R.id.chat_room_msg_tv);
            TextView readTV = (TextView)convertView.findViewById(R.id.chatroom_new_TV);
            ImageView isPictureIV = (ImageView)convertView.findViewById(R.id.is_picture_imageview);
//            readTV.setVisibility(View.VISIBLE);
            viewHolder.gridViewProfileIV = (ImageView) convertView.findViewById(R.id.chat_room_IV);
            nameTV.setText(RoomList.get(position).getOpUserName());
            String shortMsg = RoomList.get(position).getMsg();
//            Log.e("123456",shortMsg.substring(0,38));
            if (shortMsg.length() > 38){
                if (shortMsg.substring(0,38).equals("https://firebasestorage.googleapis.com")){
                    shortMsg = "picture";
                    isPictureIV.setVisibility(View.VISIBLE);
                }
            }else {
                isPictureIV.setVisibility(View.GONE);
            }
            if (shortMsg.length() > 18){
                shortMsg = shortMsg.substring(0,18) +"..";
            }
//            Log.i("2111",RoomList.get(position).isRead()+" is read");
            if (RoomList.get(position).isRead()){
                readTV.setVisibility(View.VISIBLE);
            }else{
                readTV.setVisibility(View.INVISIBLE);
            }
            msgTV.setText(shortMsg);


//            convertView.setTag(viewHolder);
//            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.imageURL = RoomList.get(position).getOpPhotoUrl();
            if (viewHolder.imageURL.toString().length() > 18) {
                String sImageName = viewHolder.imageURL.toString().substring(87, viewHolder.imageURL.toString().length());
                Log.d("=======","img url : " + viewHolder.imageURL.toString());
                Log.d("=======","img url : " + sImageName);
                if (checkExistanceOfFile(sImageName)){
                    viewHolder.gridViewProfileIV.setImageBitmap(bringImageFmLocal(sImageName));
                }
            }
            new DownloadAsyncTask().execute(viewHolder);
        }
        return convertView;
    }

    private boolean checkExistanceOfFile(String imageName){
        String path = Environment.getExternalStorageDirectory() + "/perfectDatingProfile/smallImage/"+imageName+".jpg";
        File file = new File(path);
        if(file.exists()){
            return true;
        }else {
            return false;
        }

    }
    private Bitmap bringImageFmLocal(String imagePath){
        Log.d("=======","bringImageFmLocal");
        Bitmap bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/smallImage/"+imagePath+".jpg");
        return bitmap;
    }
    private class DownloadAsyncTask extends AsyncTask<ViewHolder, Void, ViewHolder> {
        String imageName;
        @Override
        protected ViewHolder doInBackground(ViewHolder... params) {
            ViewHolder viewHolder = params[0];

            try {
                URL imageURL = new URL(viewHolder.imageURL);
                if (imageURL.toString().length() > 18) {
                    imageName = imageURL.toString().substring(87, imageURL.toString().length());
                }
                if (viewHolder.imageURL.equals("none")){
                }else{
                    viewHolder.bitmap = BitmapFactory.decodeStream(imageURL.openStream());
                    createDirectoryAndSaveFile(viewHolder.bitmap,imageName);
                }


            } catch (IOException e) {
                Log.e("error", "Downloading Image Failed");
                viewHolder.bitmap = null;
            }
            return viewHolder;
        }

        @Override
        protected void onPostExecute(ViewHolder result) {
            if (result.bitmap == null) {
//                result.gridViewProfileIV.setBackgroundColor(Color.WHITE);
                result.gridViewProfileIV.setImageResource(R.drawable.icon250);
            } else {
//                Log.d("2133 url : ", result.imageURL.toString());
                result.gridViewProfileIV.setImageBitmap(result.bitmap);

            }
        }
        private void createDirectoryAndSaveFile(Bitmap imageToSave, String fileName) {
            File direct = new File(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/smallImage/");

            if (!direct.exists()) {
                File wallpaperDirectory = new File(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/smallImage/");
                wallpaperDirectory.mkdirs();
            }

            File file = new File(new File(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/smallImage/"), fileName +".jpg");
            if (file.exists()) {
                file.delete();
            }
            try {
                FileOutputStream out = new FileOutputStream(file);
                imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        private boolean checkExistanceOfFile(String imageName){
            String path = Environment.getExternalStorageDirectory() + "/perfectDatingProfile/smallImage/"+imageName+".jpg";
            File file = new File(path);
            if(file.exists()){
                return true;
            }else {
                return false;
            }

        }
        private Bitmap bringImageFmLocal(String imagePath){
            Log.d("=======","bringImageFmLocal");
            Bitmap bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/smallImage/"+imagePath+".jpg");
            return bitmap;
        }
    }
}