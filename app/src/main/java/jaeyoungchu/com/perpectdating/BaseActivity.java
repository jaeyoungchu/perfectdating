package jaeyoungchu.com.perpectdating;

import android.app.ActivityManager;
import android.app.NotificationManager;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import jaeyoungchu.com.perpectdating.fragment.ListFragment;
import jaeyoungchu.com.perpectdating.fragment.ChatRoomListViewFragment;
import jaeyoungchu.com.perpectdating.fragment.TabFragment3;
import jaeyoungchu.com.perpectdating.fragment.TabFragment4;
import jaeyoungchu.com.perpectdating.model.ChatData;
import jaeyoungchu.com.perpectdating.model.ProfileModel;
import jaeyoungchu.com.perpectdating.services.FirebaseBackgroundService;


public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private boolean readFlag = false;
    private final String TAG = "L Base Activity Log";
    public static String previosMsg = "Dummy";
    Toolbar toolbar;
    String barTitle[] = new String[] { "Chat", "Find", "Game", "Me"  };
    String userUid,userSex,userName;
    ViewPager viewPager;
    PagerAdapter pagerAdapter;
    TabLayout tabLayout;
    NavigationView navigationView;
    ActionBarDrawerToggle toggle;
    DrawerLayout drawer;
    ProfileModel receivedProfile;
    TextView navUserNameTV,navUserSexTV;
    ImageView navProfileIV;
    private ProgressDialog mProgressDialog;
    FirebaseUser user;
    private DatabaseReference myDBRef;
    private FirebaseDatabase database;
    private int countPush=0;
//    private StorageReference pathReference;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        database = FirebaseDatabase.getInstance();
        myDBRef = FirebaseDatabase.getInstance().getReferenceFromUrl("https://perpectdating.firebaseio.com/");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        user = FirebaseAuth.getInstance().getCurrentUser();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List");

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        toggle = new ActionBarDrawerToggle(this,drawer,R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                drawer.openDrawer(GravityCompat.START);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };
        toggle.setDrawerIndicatorEnabled(true);

        drawer.addDrawerListener(toggle);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), BaseActivity.this);
        viewPager.setAdapter(pagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                getSupportActionBar().setTitle(barTitle[position]);
                if (readFlag){
                    inflateTabNone();
                    inflateTabCustomNew();
                }else {
                    inflateTabNone();
                    inflateTabCustomNone();
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }

        });

        getUserToken();
        inflateTabCustomNone();
        setTabTitle();

        View header=navigationView.getHeaderView(0);
        navUserNameTV = (TextView)header.findViewById(R.id.nav_userName_textView);
        navUserSexTV = (TextView)header.findViewById(R.id.nav_userSex_textView);
        navProfileIV = (ImageView)header.findViewById(R.id.nav_profileIV);

        myDBRef.child("profile").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                receivedProfile = dataSnapshot.child(userUid).getValue(ProfileModel.class);
                if(receivedProfile == null){
                    Log.d(TAG, "no value and first time log in");
                    startActivity(new Intent(getBaseContext(),ProfileActivity.class));
                }else{
                    savePreferences(receivedProfile);
                    Log.d(TAG, "saved shared reffer -------------------------------------");
                    userName = receivedProfile.getUsername();
                    navUserNameTV.setText(userName);
                    userSex = receivedProfile.getZender();
                    navUserSexTV.setText(userSex);
                    setImageFmLocal();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "Failed to read value.", databaseError.toException());
            }
        });


        Log.d("2133-----","is exist service ? 1 " + isMyServiceRunning(FirebaseBackgroundService.class));

        startService(new Intent(getApplicationContext(), FirebaseBackgroundService.class));
        Log.d("2133-----","is exist service ? 2 " + isMyServiceRunning(FirebaseBackgroundService.class));

    }

//    private void pushNotification(String userName, String msg){
//
//        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        Intent intent = new Intent(this, BaseActivity.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        Log.d("2111",previosMsg+",   " + msg);
//        if (!previosMsg.equals(msg)){
//            NotificationCompat.Builder mBuilder =
//                    new NotificationCompat.Builder(this)
//                            .setSmallIcon(R.drawable.ic_action_new_msg)
//                            .setContentTitle(userName)
//                            .setContentText(msg)
//                            .setContentIntent(pendingIntent)
//                            .setSound(soundUri);
//            int mNotificationId = 001;
//            mBuilder.setVibrate(new long[] { 0, 1000, 1000 });
//            mBuilder.setLights(Color.RED, 3000, 3000);
//            NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//            mNotifyMgr.notify(mNotificationId, mBuilder.build());
//            previosMsg = msg;
//        }
//    }
//
//    public static void cancelNotification(Context ctx, int notifyId) {
//        String ns = Context.NOTIFICATION_SERVICE;
//        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
//        nMgr.cancel(notifyId);
//    }

    private void setTabTitle(){
        Query queryRef = myDBRef.child("chatroom").child(userUid);
        queryRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(final DataSnapshot snapshot, String previousChild) {

                myDBRef.child("chatroom").child(snapshot.getKey()).child(userUid).orderByChild("creationDate").limitToLast(1).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        ChatData cData = dataSnapshot.getValue(ChatData.class);
                        if (cData.isRead() == true){
                            inflateAsBoolean(true);
                            readFlag = true;
//                            if (!ChatRoomActivity.inRoom){
//                                if(cData.getUserName().equals(userName)){
//                                    if (cData.isPicture()){
//                                        pushNotification(cData.getOpUserName(),"picture");
//                                    }else {
//                                        pushNotification(cData.getOpUserName(),cData.getMsg());
//                                    }
//
//                                }else{
//                                    if (cData.isPicture()){
//                                        pushNotification(cData.getUserName(),"picture");
//                                    }else{
//                                        pushNotification(cData.getUserName(),cData.getMsg());
//                                    }
//                                }
//                            }
                        }
                    }
                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        Log.d("2133---------","onChildChanged.............");
                        ChatData cData = dataSnapshot.getValue(ChatData.class);
                        if (cData.isRead() == false){
                            readFlag = false;
                            inflateAsBoolean(false);
                        }
                        if (ChatRoomActivity.inRoom && cData.getOpUserToken().equals(ChatRoomActivity.opUserTokenInNotification)){
//                            cancelNotification(getBaseContext(),001);
                        }
                    }
                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                    }
                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
            @Override
            public void onChildChanged(final DataSnapshot dataSnapshot2, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void inflateTabCustomNew(){
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            if (i==0){
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(R.layout.custom_tab_new);
            }else{
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(pagerAdapter.getTabView(i));
            }
        }
    }
    private void inflateTabCustomNone(){
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(pagerAdapter.getTabView(i));
        }
    }
    private void inflateTabNone(){
        tabLayout.setupWithViewPager(viewPager);

    }

    private void inflateAsBoolean(boolean localReadFlag){
        if (localReadFlag){
//              inflateTabNew();
            inflateTabNone();
            inflateTabCustomNew();
        }else{
            inflateTabNone();
            inflateTabCustomNone();
//            inflateTabNone();
        }
    }

    // 값 저장하기
    private void savePreferences(ProfileModel inputModel){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(inputModel);
        editor.putString("myProfile", json);
        editor.commit();
    }

    private void signOut(){
        Log.d(TAG, "inside sign out method");
        LoginManager.getInstance().logOut();
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getBaseContext(), LoginActivity.class));
        this.finish();
    }

    //

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (toggle.onOptionsItemSelected(item)) {
//            Log.d(TAG,"--------------------------------------- clicked");
            drawer.openDrawer(GravityCompat.START);
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();

    }

    private void setImageFmLocal(){
        if (receivedProfile.getSmallProfileUri().equals("none")){
            navProfileIV.setImageResource(R.drawable.icon250);
        }else{
            Bitmap bitmap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + "/perfectDatingProfile/profile.jpg");
            navProfileIV.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            startActivity(new Intent(BaseActivity.this,PhotoActivity.class));
        }
//        else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_porfile_item) {
//
//        }
        else if (id == R.id.nav_signOut) {
            signOut();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getUserToken(){
        if (user != null) {
            userUid = user.getUid();
//            Log.d(TAG,"user Token " + userUid);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("2133-----","on resume base Activicy -------------------");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d("2133-----","on stop base Activicy -------------------");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.d("2133-----","on destroy base Activicy -------------------");
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }







    class PagerAdapter extends FragmentPagerAdapter {

        String tabTitles[] = new String[] { "Chat", "Find", "Game", "Me" };
        Context context;

        public PagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public void startUpdate(ViewGroup container) {

            super.startUpdate(container);
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ChatRoomListViewFragment();
                case 1:
                    return new ListFragment();
                case 2:
                    return new TabFragment3();
                case 3:
                    return new TabFragment4();
            }
            return null;
        }


        public View getTabView(int position) {
            View tab = LayoutInflater.from(BaseActivity.this).inflate(R.layout.custom_tab, null);
            TextView readTV = (TextView) tab.findViewById(R.id.isRead_ontab_TV);
            TextView tv = (TextView) tab.findViewById(R.id.custom_text);
            Log.d("12345","get Tab View : " +position + " flag : " );
            tv.setText(tabTitles[position]);
            return tab;
        }

    }
}
